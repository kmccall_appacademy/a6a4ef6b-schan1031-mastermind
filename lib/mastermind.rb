class Code
  attr_reader :PEGS

  COLORS = [:red, :green, :blue, :yellow, :orange, :purple]
  MAP = {:R => :red, :G => :green, :B => :blue, :Y => :yellow, :O => :orange, :P => :purple}
  MAX_TURNS = 10
  PEGS = {0 =>COLORS.sample(1), 1 => COLORS.sample(1), 2 => COLORS.sample(1), 3 => COLORS.sample(1)}

  def initialize(pegs, guess = {})
    @turns = 0
    @guess = guess
    @pegs = pegs
    # @code = COLORS.sample(4)
  end

  def self.parse(str)
    # print "Enter a Guess of Four Colors"
    temp = str.upcase.split('').map {|l| MAP[l.to_sym]}
    if temp.include?(nil)
      raise 'Invalid Color'
    end
    cod = Hash[(0..3).zip(temp)]
    @code = Code.new(cod)
  end

  def self.random
    seq = COLORS.sample(4)
    Code.new(Hash[(0..3).zip(seq)])
  end

  def pegs
    @pegs
  end

  def [](key)
    @pegs[key]
  end


end

class Game
  attr_reader :secret_code
end
